(function() {
	angular.module('app', []);
    angular
        .module('app')
        .controller('CatalogueController', CatalogueController);

    function CatalogueController() {
        angular.extend(this, {
            search: '',
            catalogueItems: data
        });
    }

    var data = [
        {
            "name": "iPhone 7",
            "price": 1099
        },
        {
            "name": "YotaPhone 3",
            "price": 999
        },
        {
            "name": "iPhone 7",
            "price": 1099
        },
        {
            "name": "YotaPhone 3",
            "price": 999
        }
    ];
})();