(function() {
	angular.module('app', []);
    
    angular
        .module('app')
        .controller('ClickController', ClickController);

    function ClickController() {
        var labelText = 'Click me!';
        var clicked = 0;
        var link = '#hash';

        function updateClick() {
            this.clicked++;
        }

        angular.extend(this, {
            labelText: labelText,
            clicked: clicked,
            link: link,
            updateClick: updateClick
        });
    }
})();